function c_form(object) {
    this.request = object;

    this.open = function () {
        var user_id = this.request.user_id;
        var form_id = this.request.form_id;
        var c_form_request = new XMLHttpRequest();
        c_form_request.open('GET', 'http://localhost:8000/api/getforms?user_id='+user_id+'&uuid='+form_id, false);
        c_form_request.send();

        return c_form_request.response;
    }
}

var obj = new c_form({
    "user_id": "ad0bdd71-9567-4c89-a229-21c9a7b047d6",
    "form_id": "b44ed9b0-6f4b-4c24-b03f-7c0fcc6f73ff"
});


function required (required) {
    if(required){
        return 'required';
    }
    else {
        return '';
    }
}

var a = JSON.parse(obj.open());
var prerender = JSON.parse(a[0].content);
var createHtml = '';
var callbackDiv = document.createElement('div');
var callbackForm = document.createElement('form');
var page = document.querySelector('body');
var head = document.queryCommandEnabled('head');
var tag_css = document.createElement('link');
var tag_head = document.getElementsByTagName('head')


callbackDiv.className = 'callback_div';
callbackForm.className = "callback_form";
callbackForm.id = 'callback_form';
callbackForm.method = 'post';


for (var i = 0; i < prerender.length; i++){
    createHtml += '<label>' + prerender[i].label + '</label>';
    createHtml += '<'
        + prerender[i].tagName + ' name="'
        + prerender[i].name + '" placeholder="'+prerender[i].placeholder+'" '+ required(prerender[i].required)+' ';
    if(prerender[i].type !== "") {
        createHtml += 'type="' + prerender[i].type + '">';
    }
    else {
        createHtml += '>'
    }

    if (prerender[i].tagName === "textarea"){
        createHtml += '</textarea>'
    }
}

createHtml += '<p class="webpush"><input name="push" id="push" type="checkbox"> Получить push - уведомление</p>'
createHtml += '<button type="submit">Отправить</button>';
callbackForm.innerHTML = createHtml;
callbackDiv.insertBefore(callbackForm, null);
page.insertBefore(callbackDiv, null);


tag_css.type = 'text/css';
tag_css.rel = 'stylesheet';
tag_css.href = 'http://localhost:8000/main.css?get=' + obj.request.form_id;

tag_head[0].appendChild(tag_css);

document.getElementById('callback_form').addEventListener(
    'submit',
    function (e) {
        e.preventDefault();
    },
    false
);