var myCodeMirror;
$( function() {
    $( "#sortable" ).sortable({
        revert: true
    });
    var counter = 0;
    $( ".draggable" ).draggable({
        connectToSortable: "#sortable",
        helper: "clone",
        revert: "invalid",
        stop: function( event, ui ) {
            $('.codemirror').css({display: "block"});
            if(!myCodeMirror) {
                myCodeMirror = CodeMirror.fromTextArea(document.getElementById('codemirror'), {
                    autoCloseBrackets: true,
                    lineNumbers: true,
                    matchBrackets: true,
                    mode: 'css',
                    indentUnit: 4
                });
            }
            $(ui.helper).children('textarea, input').attr('name', 'form_component_'+ counter);
            $(ui.helper).children('.name-text').val('form_component_'+ counter);
            counter++;
        }
    });
    $( "form" ).disableSelection();
} );
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$('.control-label').on('click', function() {
    var content = $(this).text();
    $(this).css({'display': 'none'});
    $(this).parent().append('<input type="text">');
});

$('.create_form').submit(function (e) {
    if ($('#form_name').val() == '') {
        alert('Введите имя формы');
        return false;
    }
    e.preventDefault();
    var array_components = [];
    var components = $('.create_form .form-component');

    for(var i = 0; i < components.length; i++){
        array_components.push(
            {
                "tagName": $(components[i]).children('input.form-control, textarea.form-control')[0].nodeName.toLowerCase(),
                "type": $($(components[i]).children('input.form-control, textarea.form-control')[0]).attr('type') || '',
                "name": $($(components[i]).children('input.name-text')[0]).val() || '',
                "required": $($(components[i]).children('input[type=checkbox]')[0]).prop('checked') || false,
                "placeholder": $($(components[i]).children('input.form-control, textarea.form-control')[0]).val() || '',
                "label": $($(components[i]).children('input.label-text')[0]).val() || ''
            }
        );
    }
    myCodeMirror.save();
    $.ajax({
       type: 'POST',
       url: "/create",
       data: {
           _token: $('meta[name="csrf-token"]').attr('content'),
           "name": $('#form_name').val(),
           "content": JSON.stringify(array_components),
           "css": $('#codemirror').val()
       },
       success: function () {
           console.log("OK");
           alert('Success');
       },
       error: function () {
           console.log("error");
       }
    });
});
