@extends('layouts.log')

@section('content')
            <!-- Row -->
            <div class="row">

                <div class="col-sm-offset-2 col-sm-8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body pb-0">
                                        <div class="tab-struct custom-tab-1">
                                            <ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">




                                                <li role="presentation" class="active"><a data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="true"><span>settings</span></a></li>

                                            </ul>
                                            <div class="tab-content" id="myTabContent_8">





                                                <div id="settings_8" class="tab-pane fade active in" role="tabpanel">
                                                    <!-- Row -->
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="">
                                                                <div class="panel-wrapper collapse in">
                                                                    <div class="panel-body pa-0">
                                                                        <div class="col-sm-12 col-xs-12">
                                                                            <div class="form-wrap">
                                                                                <form action="{{ route('update') }}" method="post">
                                                                                    {{ csrf_field() }}
                                                                                    <div class="form-body overflow-hide">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputuname_01">Name</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                                                                <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="exampleInputuname_01" placeholder="Имя">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputEmail_01">Email address</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                                                                <input type="email" name="email" value="{{$user->email}}" class="form-control" id="exampleInputEmail_01" placeholder="EMAIL">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputContact_01">Contact number</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-phone"></i></div>
                                                                                                <input type="text" name="phone" value="{{$user->phone}}" class="form-control" id="exampleInputContact_01" placeholder="Телефон">
                                                                                            </div>
                                                                                        </div>
                                                                                        <!--<div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputpwd_01">Password</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-lock"></i></div>
                                                                                                <input type="password" name="password" class="form-control" id="exampleInputpwd_01" placeholder="Пароль">
                                                                                            </div>
                                                                                        </div>-->
                                                                                    </div>
                                                                                    <div class="form-actions mt-10">
                                                                                        <button type="submit" class="btn btn-default mr-10 mb-30">Update profile</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="panel panel-default border-panel card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">todo</h6>
                                    </div>
                                    <div class="pull-right">
                                        <div class="pull-left inline-block dropdown mr-15">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
                                            <ul class="dropdown-menu bullet dropdown-menu-right" role="menu">
                                                <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Edit</a></li>
                                                <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>Clear All</a></li>
                                                <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>Select All</a></li>
                                            </ul>
                                        </div>
                                        <a class="pull-left inline-block close-panel" href="#" data-effect="fadeOut">
                                            <i class="zmdi zmdi-close"></i>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body row pa-0">
                                        <div class="todo-box-wrap">
                                            <!-- Todo-List -->
                                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 365px;"><ul class="todo-list todo-box-nicescroll-bar" style="overflow: hidden; width: auto; height: 365px;">
                                                    <li class="todo-item">
                                                        <div class="checkbox checkbox-default">
                                                            <input type="checkbox" id="checkbox001">
                                                            <label for="checkbox001">Record The First Episode</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <hr class="light-grey-hr">
                                                    </li>
                                                    <li class="todo-item">
                                                        <div class="checkbox checkbox-pink">
                                                            <input type="checkbox" id="checkbox002">
                                                            <label for="checkbox002">Prepare The Conference Schedule</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <hr class="light-grey-hr">
                                                    </li>
                                                    <li class="todo-item">
                                                        <div class="checkbox checkbox-warning">
                                                            <input type="checkbox" id="checkbox003" checked="">
                                                            <label for="checkbox003">Decide The Live Discussion Time</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <hr class="light-grey-hr">
                                                    </li>
                                                    <li class="todo-item">
                                                        <div class="checkbox checkbox-success">
                                                            <input type="checkbox" id="checkbox004" checked="">
                                                            <label for="checkbox004">Prepare For The Next Project</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <hr class="light-grey-hr">
                                                    </li>
                                                    <li class="todo-item">
                                                        <div class="checkbox checkbox-danger">
                                                            <input type="checkbox" id="checkbox005" checked="">
                                                            <label for="checkbox005">Finish Up AngularJs Tutorial</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <hr class="light-grey-hr">
                                                    </li>
                                                    <li class="todo-item">
                                                        <div class="checkbox checkbox-purple">
                                                            <input type="checkbox" id="checkbox006" checked="">
                                                            <label for="checkbox006">Finish Infinity Project</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <hr class="light-grey-hr">
                                                    </li>
                                                </ul><div class="slimScrollBar" style="background: rgb(135, 135, 135); width: 4px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 365px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                            <!-- /Todo-List -->

                                            <!-- New Todo -->
                                            <div class="new-todo">
                                                <div class="input-group">
                                                    <input type="text" id="add_todo" name="example-input2-group2" class="form-control" placeholder="Add new task">
                                                    <span class="input-group-btn">
														<button type="button" class="btn btn-success"><i class="zmdi zmdi-plus txt-success"></i></button>
														</span>
                                                </div>
                                            </div>
                                            <!-- /New Todo -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="panel panel-default card-view">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="calendar-wrap">
                                            <div id="calendar_small" class="small-calendar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
@endsection