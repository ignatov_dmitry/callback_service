.callback_div{
position: fixed;
bottom: 5px;
right: 5px;
width: 260px;
border: 1px solid #c0c0c0;
z-index: 1;
padding: 10px;
background: #fff;
}


.callback_form input, .callback_form textarea{
width: 100%;
margin: 10px 0;
border-radius: 5px;
border: 1px solid #999;
resize: none;
padding: 5px 0px 5px 0px;
}

.callback_form textarea{
height: 70px;
}
.callback_form button{
background: #e2e2e2;
border: none;
border-radius: 3px;
width: 100%;
height: 38px;
}
.callback_form button:active{
background: #f1f1f1;
}
.webpush #push {
width: inherit;
margin: 0 0 15px 0px;
}

.webpush{
line-height: 0.01;
}
