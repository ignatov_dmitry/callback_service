@extends('layouts.app')

@section('content')
<div class="page-wrapper pa-0 ma-0 auth-page">
    <div class="container">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float card-view pt-30 pb-30">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-dark mb-10">Sign up to Splasher</h3>
                                <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                            </div>
                            <div class="form-wrap">
                                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="exampleInputName_1">Username</label>
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                    <!--<div class="form-group">
                                        <div class="checkbox checkbox-primary pr-10 pull-left">
                                            <input id="checkbox_2" required="" type="checkbox">
                                            <label for="checkbox_2"> I agree to all <span class="txt-danger">Terms</span></label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>-->
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-danger btn-rounded">{{ __('Register') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

</div>
@endsection
