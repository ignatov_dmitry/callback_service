@extends('layouts.log')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Мои формы</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap mt-40">
                            <div class="table-responsive">

                                    @foreach($userForms as $userForm)
                                    <div class="item">
                                    <a href="{{ route('getForm') }}/{{ $userForm->uuid }}">{{ $userForm->name }}</a>
                                    <form action="{{route('delete')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{$userForm->uuid}}">
                                        <input class="btn btn-danger" type="submit" value="Удалить">
                                    </form>
                                    </div>
                                    @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <h6 class="panel-title txt-dark">Конструктор</h6>
                    <div class="form-group">
                        <input type="text" required id="form_name" name="form_name" class="form-control" placeholder="Название формы">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <form class="create_form">
                            <ul id="sortable"></ul>
                            <button type="submit" class="btn btn-default">Send</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default border-panel card-view codemirror">
                <div class="panel-heading">
                    <h6 class="panel-title txt-dark">CSS стили</h6>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <textarea name="css" id="codemirror" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Элементы формы</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="draggable form-group form-component">
                            <div class="form-element"></div>
                            <label class="control-label mb-10 text-left">Текстовое поле</label>
                            <input autocomplete="off" type="text" class="label-text" placeholder="текст над элементом">
                            <input autocomplete="off" type="text" class="name-text" placeholder="Имя компонента(Для обработки формы)">
                            <input autocomplete="off" type="text" placeholder="Введите название элемента" class="form-control">
                            <input type="checkbox"> Обязательное поле
                        </div>
                        <div class="draggable form-group form-component">
                            <div class="form-element"></div>
                            <label class="control-label mb-10 text-left">Почта</label>
                            <input autocomplete="off" type="text" class="label-text" placeholder="текст над элементом">
                            <input autocomplete="off" type="text" class="name-text" placeholder="Имя компонента(Для обработки формы)">
                            <input autocomplete="off" type="email" placeholder="Введите название элемента" class="form-control">
                            <input type="checkbox"> Обязательное поле
                        </div>
                        <div class="draggable form-group form-component">
                            <div class="form-element"></div>
                            <label class="control-label mb-10 text-left">Дата</label>
                            <input autocomplete="off" type="text" class="label-text" placeholder="текст над элементом">
                            <input autocomplete="off" type="text" class="name-text" placeholder="Имя компонента(Для обработки формы)">
                            <input autocomplete="off" type="date" placeholder="Введите название элемента" class="form-control">
                            <input type="checkbox"> Обязательное поле
                        </div>
                        <div class="draggable form-group form-component">
                            <div class="form-element"></div>
                            <label class="control-label mb-10 text-left">Большое текстовое поле</label>
                            <input autocomplete="off" type="text" class="label-text" placeholder="текст над элементом">
                            <input autocomplete="off" type="text" class="name-text" placeholder="Имя компонента(Для обработки формы)">
                            <textarea class="form-control" placeholder="Введите название элемента"></textarea>
                            <input type="checkbox"> Обязательное поле
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="{{ asset('js/form.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">



    <script src="{{ asset('plugins/codemirror/codemirror.js') }}"></script>
    <script src="{{ asset('plugins/codemirror/css.js') }}"></script>
    <script src="{{ asset('plugins/codemirror/closebrackets.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('plugins/codemirror/codemirror.css') }}">
@endsection
