<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('main.css', 'CssController@get')->name('css');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth'], function (){
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::post('/update', 'HomeController@updateProfile')->name('update');

    Route::post('/create', 'FormController@create')->name('create');
    Route::get('/form', 'FormController@index')->name('form');

    Route::post('/delete', 'FormController@delete')->name('delete');
    Route::post('/update', 'FormController@update')->name('update');
    Route::get('/getform', 'FormController@getForm')->name('getForm');

});