<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebPush;

class PushController extends Controller
{
    public function sendtoken(Request $request){
        $push = new WebPush();
        $push->token = $request['token'];
        $push->email = '1@mail.ru';
        $push->save();
        return response()->json('success');
    }
}
