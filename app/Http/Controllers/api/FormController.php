<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Form;
class FormController extends Controller
{
    public function getForms(Request $request){
        $userForms = Form::where('user_id', '=', $request['user_id'])->where('uuid', '=', $request['uuid'])->get(array('content'));
        return response()->json($userForms);
    }
}
