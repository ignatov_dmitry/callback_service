<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home',[
            'user' => Auth::user()
        ]);
    }


    public function profile(){
        $user = Auth::user();
        return view('profile',['user' => $user]);
    }

    protected function updateProfile(Request $user){
        $profile = User::find(Auth::id());
        $profile->name = $user->name;
        $profile->email = $user->email;
        $profile->phone = $user->phone;
        $profile->save();
        return redirect()->back();
    }

}
