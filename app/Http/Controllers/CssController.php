<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;
class CssController extends Controller
{
    public function get(Request $request){
        $css = Form::where('uuid', $request->get)->first();
        return response($css->css, 200)->header('Content-Type', 'text/css');
    }
}