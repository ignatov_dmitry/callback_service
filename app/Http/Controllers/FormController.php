<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Form;
class FormController extends Controller
{
    public function index(){
        $userForms = Form::where('user_id', '=', Auth::id())->get();
        return view('form',[
            'userForms' => $userForms
        ]);
    }

    public function getForm(Request $request){
        $form = Form::where('uuid', $request->get('uuid'))->first();
        dd($request);
    }


    public function create(Request $request){
        $form = new Form();
        $form->name = $request->name;
        $form->content = $request->content;
        $form->user_id = Auth::id();
        $form->css = $request->css;
        $form->save();
    }

    public function delete(Request $request){
        Form::where('uuid', $request->get('uuid'))->delete();
        return redirect()->back();
    }

    public function update(Request $request){
        $uuid = $request->get('uuid');
        Form::where('uuid', $uuid);
    }
}
